package com.educacionit.libros.db.pojo

import com.j256.ormlite.table.DatabaseTable
import com.j256.ormlite.field.DatabaseField
import java.io.Serializable

@DatabaseTable(tableName = "Libros")
data class Libro(
    @DatabaseField(generatedId = true)
    var id: Int? = null,
    @DatabaseField
    var nombre: String? = null,
    @DatabaseField
    var autor: String? = null
)
    : Serializable {
    constructor() : this(Int.MIN_VALUE, "","")

    constructor(nombre:String, autor: String?) : this(Int.MIN_VALUE, nombre, autor)


    /*
    constructor() {}
constructor(nombre: String?, autor: String?) {
    this.nombre = nombre
    this.autor = autor
}

constructor(id: Int?, nombre: String?, autor: String?) {
    this.id = id
    this.nombre = nombre
    this.autor = autor
}
     */
}