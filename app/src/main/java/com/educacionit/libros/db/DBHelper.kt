package com.educacionit.libros.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.educacionit.libros.db.pojo.Libro
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import timber.log.Timber
import java.sql.SQLException

class DBHelper(context: Context):
    OrmLiteSqliteOpenHelper(context, NOMBRE_DB,null, VERSION_DB) {

        private companion object{
            const val NOMBRE_DB = "Libros"
            const val VERSION_DB = 1
        }

    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        try {
            TableUtils.createTable(connectionSource, Libro::class.java)
        }catch (e: SQLException){
            Timber.e(e)
        }
    }

    override fun onUpgrade(
        database: SQLiteDatabase?,
        connectionSource: ConnectionSource?,
        oldVersion: Int,
        newVersion: Int
    ) {
        TODO("Not yet implemented")
    }


}