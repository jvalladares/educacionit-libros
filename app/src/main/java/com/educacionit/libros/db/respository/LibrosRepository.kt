package com.educacionit.libros.db.respository

import android.content.Context
import com.educacionit.libros.db.DBHelper
import com.educacionit.libros.db.pojo.Libro
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.dao.Dao
import timber.log.Timber
import java.sql.SQLException

class LibrosRepository(context: Context) {

    private lateinit var dao: Dao<Libro, Int>

    init {
        val helper = OpenHelperManager.getHelper(context, DBHelper::class.java)
        try{
            dao = helper.getDao(Libro::class.java)
        }catch (e:SQLException){
            Timber.e(e)
        }
    }

    fun getLibros(): List<Libro>{
        return dao.queryForAll()
    }

    fun agregarLibro(libro: Libro){
        dao.create(libro)
        Timber.i("Se inserto en la BD ${libro.toString()}")
    }

}